//
//  MovieDetailViewController.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 26.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import UIKit
import AVKit
import YoutubeDirectLinkExtractor

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieGenresLabel: UILabel!
    @IBOutlet weak var movieDateLabel: UILabel!
    @IBOutlet weak var movieOverviewLabel: UILabel!
    
    let viewModel = MovieDetailViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         self.viewModel.loadFilmDetail { _,_  in
            self.updateFilmInfo()
        }
        self.viewModel.loadFilmTrailers { _,_ in
            
        }
    }
    
    func updateFilmInfo() {
        self.movieTitleLabel.text = self.viewModel.film?.title ?? ""
        self.movieOverviewLabel.text = self.viewModel.film?.overview ?? ""
        let fullUrl = kImageBaseUrlString + PhotoBackgroundResolutionName.wide1280.rawValue + (self.viewModel.film!.backDropPath ?? "")
        MFNetworkManager.sharedManager.setPhotoImageView(self.movieImageView, photoUrl: fullUrl)
        self.movieGenresLabel.text = self.viewModel.getFilmsGenres()
        self.movieDateLabel.text = self.viewModel.getFilmDate()
    }
    
    @IBAction func watchTrailerButtonPressed(_ sender: Any) {
        let youtubeExtractor = YoutubeDirectLinkExtractor()
        if let key = self.viewModel.film!.trailerPath {
            youtubeExtractor.extractInfo(for: .id(key), success: { info in
                let player = AVPlayer(url: URL(string: info.highestQualityPlayableLink!)!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
            }) { error in
                print(error)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}
