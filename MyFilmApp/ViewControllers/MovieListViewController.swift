//
//  MovieListViewController.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 26.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController {

    @IBOutlet weak var movieSearchBar: UISearchBar!
    @IBOutlet weak var movieListTableView: UITableView!
    
    var gestureRecognizer: UITapGestureRecognizer?
    
    let viewModel = MoviesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loadFilms()
        
        self.registerKeyboardNotifications()
    }
    
    func loadFilms() {
        self.viewModel.getFilmsList { _,_  in
            self.movieListTableView.reloadData()
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showDetailSegue" {
            let filmDetailController = segue.destination as! MovieDetailViewController
            let index = self.movieListTableView.indexPath(for: sender as! MovieTableViewCell)
            if (index != nil) {
                filmDetailController.viewModel.film = self.viewModel.filmsList[index!.row]
            }
        }
    }
}

extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.filmsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        let film = self.viewModel.filmsList[indexPath.row]
        cell.setCellTitle(film.title)
        cell.setWideCellPhotoImage(photoUrl: film.backDropPath)
        return cell
    }
}

extension MovieListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.bounds.maxY == scrollView.contentSize.height) {
            if Connectivity.isConnectedToInternet {
                self.viewModel.increasePage()
                self.loadFilms()
            }
        }
    }
}

extension MovieListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.searchString = searchText
        self.movieListTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if self.movieSearchBar.isFirstResponder {
            self.movieSearchBar.resignFirstResponder()
        }
    }
}

extension MovieListViewController {
    func registerKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(notif:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notif: Notification?) {
        if notif != nil, let endFrame = notif?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            if endFrame.origin.y < self.movieListTableView.bounds.size.height {
                self.addKeyboardHidingGesture()
                UIView.animate(withDuration: 0.25, animations: {
                    self.movieListTableView.frame = CGRect(x: self.movieListTableView.frame.origin.x, y: self.movieListTableView.frame.origin.y, width: self.movieListTableView.frame.width, height: self.movieListTableView.frame.height - endFrame.height)
                })
            } else {
                UIView.animate(withDuration: 0.25, animations: {
                    self.movieListTableView.frame = CGRect(x: self.movieListTableView.frame.origin.x, y: self.movieListTableView.frame.origin.y, width: self.movieListTableView.frame.width, height: self.movieListTableView.frame.height + endFrame.height)
                }) { _ in
                    self.removeKeyboardHidingGesture()
                }
            }
        }
    }
}

extension MovieListViewController {
    
    func addKeyboardHidingGesture() {
        self.gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        self.view.addGestureRecognizer(gestureRecognizer!)
    }
    
    func removeKeyboardHidingGesture() {
        guard self.gestureRecognizer != nil else { return }
        self.view.removeGestureRecognizer(self.gestureRecognizer!)
        self.gestureRecognizer = nil
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
}
