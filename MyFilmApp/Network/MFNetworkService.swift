//
//  MFNetworkService.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 27.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import UIKit
import Alamofire

let kBaseUrlString: String = "https://api.themoviedb.org/3"
let kApiKeyString: String = "571c824763aca739eca606b212602eb5"

let kImageBaseUrlString: String = "https://image.tmdb.org/t/p/"

public enum BackendError: Error {
    case Default(message: String)
    case TimeOut
    case NoInternet
}

typealias Success = (Result<Any>) -> ()
typealias Failure = (BackendError) -> ()

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}

class MFNetworkService {
    static let sharedService = MFNetworkService()
    
    func checkBackendError(response: DataResponse<Any>?) -> BackendError? {
        switch response!.result {
        case .success( _):
            return nil
        case .failure(let error):
            if let err = error as? URLError, err.code == .notConnectedToInternet {
                return .NoInternet
            }
            if let err = error as? URLError, err.code == .timedOut {
                return .TimeOut
            }
        }
        return nil
    }
    
    func getFilms(page: Int = 1, success: @escaping Success, failure: @escaping Failure) {
        let parameters: Parameters = [
            "api_key": kApiKeyString,
            "page": page
        ]
        Alamofire.request("\(kBaseUrlString)/movie/popular",  method: .get, parameters: parameters, encoding: URLEncoding.queryString).responseJSON(completionHandler: { (response) in
            if let error = self.checkBackendError(response: response) {
                failure(error)
            } else {
                success(response.result)
            }
        })
    }
    
    func getFilmBy(filmId: Int, success: @escaping Success, failure: @escaping Failure) {
        let parameters: Parameters = [
            "api_key": kApiKeyString
        ]
        Alamofire.request("\(kBaseUrlString)/movie/\(filmId)",  method: .get, parameters: parameters, encoding: URLEncoding.queryString).responseJSON(completionHandler: { (response) in
            if let error = self.checkBackendError(response: response) {
                failure(error)
            } else {
                success(response.result)
            }
        })
    }
    
    func getFilmMoviesBy(filmId: Int, success: @escaping Success, failure: @escaping Failure) {
        let parameters: Parameters = [
            "api_key": kApiKeyString
        ]
        Alamofire.request("\(kBaseUrlString)/movie/\(filmId)/videos",  method: .get, parameters: parameters, encoding: URLEncoding.queryString).responseJSON(completionHandler: { (response) in
            if let error = self.checkBackendError(response: response) {
                failure(error)
            } else {
                success(response.result)
            }
        })
    }
}
