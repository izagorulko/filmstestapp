//
//  PhotoResolutionName.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 02.04.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import Foundation

public enum PhotoBackgroundResolutionName: String {
    case wide300 = "w300"
    case wide780 = "w780"
    case wide1280 = "w1280"
    case original = "original"
}
