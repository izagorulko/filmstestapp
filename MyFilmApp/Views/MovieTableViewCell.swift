//
//  MovieTableViewCell.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 26.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import UIKit
import AlamofireImage

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellTitle(_ title: String?) {
        self.movieTitleLabel.text = title ?? ""
    }
    
    func setWideCellPhotoImage(photoUrl: String?) {
        let fullUrl = kImageBaseUrlString + PhotoBackgroundResolutionName.wide300.rawValue + photoUrl!
        MFNetworkManager.sharedManager.setPhotoImageView(self.movieImageView, photoUrl: fullUrl)
    }
}
