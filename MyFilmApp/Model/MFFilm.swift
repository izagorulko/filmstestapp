//
//  MFFilm.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 27.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

class MFFilm: Object {
    @objc dynamic var id = 0
    @objc dynamic var title: String?
    @objc dynamic var backDropPath: String?
    @objc dynamic var overview: String?
    @objc dynamic var releaseDateString: String?
    @objc dynamic var trailerPath: String?
    
    var genresList = List<String?>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func createFilmWith(json: JSON) {
        id = json["id"].int!
        title = json["title"].string ?? ""
        backDropPath = json["backdrop_path"].string ?? ""
        overview = json["overview"].string ?? ""
        releaseDateString = json["release_date"].string ?? ""
    }
    
    func updateFilmData(_ json: JSON) {
        title = json["title"].string ?? title
        backDropPath = json["backdrop_path"].string ?? backDropPath
        overview = json["overview"].string ?? overview
        releaseDateString = json["release_date"].string ?? releaseDateString
        if let genresArray = json["genres"].array {
            let resultArray = genresArray.map { return $0["name"].string }
            genresList.removeAll()
            genresList.append(objectsIn: resultArray)
        }
    }
    
    func updateFilmTrailerPath(_ json: JSON) {
        if let resultsArray = json["results"].array {
            let trailerJson = resultsArray.filter { return $0["name"].string == "Official Trailer" }.last!
            trailerPath = trailerJson["key"].string ?? ""
        }
    }
}
