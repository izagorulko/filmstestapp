//
//  MovieDetailViewModel.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 28.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import UIKit

class MovieDetailViewModel {
    
    var film: MFFilm? = nil {
        didSet {
            self.filmId = film?.id
        }
    }
    
    var filmId: Int?
    
    func loadFilmDetail(complete: @escaping CompleteHandler) {
        MFNetworkManager.sharedManager.getFilmDetailsBy(filmId: self.filmId!) { (result, error) in
//            if (result) {
                self.film = MFRepositoryManager.sharedManager.getFilmBy(filmId: self.filmId!)
//            }
            complete(result, error)
        }
    }
    
    func loadFilmTrailers(complete: @escaping CompleteHandler) {
        MFNetworkManager.sharedManager.getFilmTrailerBy(filmId: self.filmId!) { (result, error) in
            self.film = MFRepositoryManager.sharedManager.getFilmBy(filmId: self.filmId!)
            complete(result, error)
        }
    }
    
    func getFilmsGenres() -> String {
        if let genres = self.film?.genresList.toArray() {
            let resArray = genres.map({ $0! })
            return resArray.joined(separator:", ")
        }
        return ""
    }
    
    func getFilmDate() -> String {
        guard self.film != nil else { return "" }
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        let filmDate = dayTimePeriodFormatter.date(from: self.film!.releaseDateString!)
         dayTimePeriodFormatter.dateFormat = "dd.MM.YYYY"
        return dayTimePeriodFormatter.string(from: filmDate!)
    }
}
