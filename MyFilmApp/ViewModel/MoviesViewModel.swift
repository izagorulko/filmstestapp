//
//  MoviesViewModel.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 28.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import UIKit

class MoviesViewModel {
    
    var filmsList: [MFFilm] = []
    var page: Int = 1
    var searchString: String? = nil {
        didSet {
            self.filmsList = self.searchFilm(searchString ?? "")
        }
    }
    
    func increasePage() {
        if MFRepositoryManager.sharedManager.countPages! > self.page {
            self.page += 1
        }
    }
    
    func getFilmsList(complete: @escaping CompleteHandler) {
        MFNetworkManager.sharedManager.getFilms(page: self.page, complete: { (result, errorString) in
//            if (result) {
                self.filmsList = MFRepositoryManager.sharedManager.getFilms()
//            }
            complete(result, errorString)
        })
    }
    
    func searchFilm(_ searchStr: String) -> [MFFilm] {
        guard searchStr != "" else {
            return MFRepositoryManager.sharedManager.getFilms()
        }
        return MFRepositoryManager.sharedManager.searchFilmBy(nameStr: searchStr)
    }
}
