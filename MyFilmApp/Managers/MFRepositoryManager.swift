//
//  MFRepositoryManager.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 27.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

let kMFRepositoryName: String = "myFilmsAppBase.realm"

extension Results {
    
    func toArray() -> [Element] {
        return self.map{$0}
    }
}

extension RealmSwift.List {
    
    func toArray() -> [Element] {
        return self.map{$0}
    }
}

class MFRepositoryManager {
    static let sharedManager = MFRepositoryManager()
    
    var countPages: Int? = 1
    
    init() {
        self.configurateRealmDataBase()
    }
    
    // Configurate Realm Data Base
    func configurateRealmDataBase() {
        var config = Realm.Configuration()
        config.schemaVersion = 1
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent(kMFRepositoryName)
        Realm.Configuration.defaultConfiguration = config
    }
    
    func saveFilms(_ filmsData: JSON) {
        let realm = try! Realm()
        let filmsArray = filmsData["results"].array!
        self.countPages = filmsData["total_pages"].int
        try! realm.write {
            for filmJson in filmsArray {
                let filmId = filmJson["id"].int
                if let cFilm = self.getFilmBy(filmId: filmId!) {
                    cFilm.updateFilmData(filmJson)
                } else {
                    let film = MFFilm()
                    film.createFilmWith(json: filmJson)
                    realm.add(film, update: true)
                }
            }
        }
    }
    
    func saveFilmDetails(_ filmData: JSON) {
        let realm = try! Realm()
        let filmId = filmData["id"].int
        try! realm.write {
            if let cFilm = self.getFilmBy(filmId: filmId!) {
                cFilm.updateFilmData(filmData)
            }
        }
    }
    
    func saveFilmTrailer(_ filmData: JSON) {
        let realm = try! Realm()
        let filmId = filmData["id"].int
        try! realm.write {
            if let cFilm = self.getFilmBy(filmId: filmId!) {
                cFilm.updateFilmTrailerPath(filmData)
            }
        }
    }
    
    func getFilms() -> [MFFilm] {
        let result = try! Realm().objects(MFFilm.self)
        guard result.count > 0 else { return [] }
        return result.toArray()
    }
    
    // Get film by ID
    func getFilmBy(filmId: Int) -> MFFilm? {
        let result = try! Realm().objects(MFFilm.self).filter("id == %d", filmId)
        guard result.count > 0 else { return nil }
        return result.first
    }
    
    func searchFilmBy(nameStr: String) -> [MFFilm] {
        let results = try! Realm().objects(MFFilm.self).filter("title CONTAINS[c] %@", nameStr).sorted(byKeyPath: "id")
        return results.toArray()
    }
}
