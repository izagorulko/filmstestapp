//
//  MFNetworkManager.swift
//  MyFilmApp
//
//  Created by Ivan Zagorulko on 27.03.2019.
//  Copyright © 2019 GarnetSoft. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias CompleteHandler = (Bool, String?) -> ()

class MFNetworkManager {
    static let sharedManager = MFNetworkManager()
    
    func getErrorMessageFrom(error: BackendError) -> String {
        switch error {
        case .NoInternet:
            return "No internet connection"
        case .TimeOut:
            return "Request time out"
        case .Default(let message):
            return message
        }
    }
    
    func getFilms(page: Int = 1, complete: @escaping CompleteHandler) {
        MFNetworkService.sharedService.getFilms(page: page, success: { (result) in
            let resultValue = result.value as AnyObject
            MFRepositoryManager.sharedManager.saveFilms(JSON(resultValue))
            complete(true, nil)
        }) { (error) in
            let errorString = self.getErrorMessageFrom(error: error)
            complete(false, errorString)
        }
    }
    
    func getFilmDetailsBy(filmId: Int, complete: @escaping CompleteHandler) {
        MFNetworkService.sharedService.getFilmBy(filmId: filmId, success: { (result) in
            let resultValue = result.value as AnyObject
            MFRepositoryManager.sharedManager.saveFilmDetails(JSON(resultValue))
            complete(true, nil)
        }) { (error) in
            let errorString = self.getErrorMessageFrom(error: error)
            complete(false, errorString)
        }
    }
    
    func getFilmTrailerBy(filmId: Int, complete: @escaping CompleteHandler) {
        MFNetworkService.sharedService.getFilmMoviesBy(filmId: filmId, success: { (result) in
            let resultValue = result.value as AnyObject
            MFRepositoryManager.sharedManager.saveFilmTrailer(JSON(resultValue))
            complete(true, nil)
        }) { (error) in
            let errorString = self.getErrorMessageFrom(error: error)
            complete(false, errorString)
        }
    }
    
    func loadImageFromCashWith(path: String?) -> UIImage? {
        let imageCache = PersistentAutoPurgingImageCache.default
        var image: UIImage?
        if let linkUrlString = path {
            image = imageCache.image(withIdentifier: linkUrlString)
        }
        return image
    }
    
    func saveImageToCashWith(path: String?, image: UIImage) {
        let imageCache = PersistentAutoPurgingImageCache.default
        imageCache.add(image, withIdentifier: path!)
    }
    
    func setPhotoImageView(_ imageView: UIImageView, photoUrl: String?) {
        if let image = self.loadImageFromCashWith(path: photoUrl) {
            imageView.image = image
        } else {
            imageView.image = nil
            imageView.af_setImage(withURL: URL(string: photoUrl!)!, completion: { dataResponse in
                if let image = dataResponse.result.value {
                    self.saveImageToCashWith(path: photoUrl, image: image)
                }
            })
        }
    }
}
